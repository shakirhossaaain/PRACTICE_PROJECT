<?php
    session_start();
    require_once 'database.php';


    if (isset($_SESSION['login'])){
        header("location:contact_view.php");
    }

?>

<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>LOGIN PAGE</title>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <h1>LOGIN</h1>

                    <form style="float:right" action="registration.php" >
                      <button  class="btn btn-info" type="submit" name = "registration" value = "one">REGISTRATION</button>
                    </form>

                    <form action="login_check.php" method="post">
                        <div class="form-group">
                            <label for="">Email</label>
                            <input class="form-control" type="email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="">Password</label>
                            <input class="form-control" type="password" name="password">
                        </div>
                        <div class="form-group">
                            <button class="form-control btn btn-success" type="submit">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
