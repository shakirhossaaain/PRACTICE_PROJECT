<?php
    //var_dump($_SERVER);
   // echo $_SERVER["PHP_SELF"];
    $link = explode('/',$_SERVER["PHP_SELF"]);
    //var_dump($link);
    $page=$link[2];
    //echo $page;
?>

<!DOCTYPE html>


<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Enlight </title>
    <meta name="description" content="Free Bootstrap Theme by ProBootstrap.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,700|Open+Sans" rel="stylesheet">
    <link rel="stylesheet" href="css/styles-merged.css">
    <link rel="stylesheet" href="css/style.min.css">
    <link rel="stylesheet" href="css/custom.css">
    <script src="js/vendor/html5shiv.min.js"></script>
    <script src="js/vendor/respond.min.js"></script>
</head>
<body>

<div class="probootstrap-search" id="probootstrap-search">
    <a href="#" class="probootstrap-close js-probootstrap-close"><i class="icon-cross"></i></a>
    <form action="#">
        <input type="search" name="s" id="search" placeholder="Search a keyword and hit enter...">
    </form>
</div>

<div class="probootstrap-page-wrapper">
    <!-- Fixed navbar -->

    <div class="probootstrap-header-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-9 probootstrap-top-quick-contact-info">
                    <span><i class="icon-location2"></i>Brooklyn, NY 10036, United States</span>
                    <span><i class="icon-phone2"></i>+1-123-456-7890</span>
                    <span><i class="icon-mail"></i>info@probootstrap.com</span>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 probootstrap-top-social">
                    <ul>
                        <li><a href="#"><i class="icon-twitter"></i></a></li>
                        <li><a href="#"><i class="icon-facebook2"></i></a></li>
                        <li><a href="#"><i class="icon-instagram2"></i></a></li>
                        <li><a href="#"><i class="icon-youtube"></i></a></li>
                        <li><a href="#" class="probootstrap-search-icon js-probootstrap-search"><i class="icon-search"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-default probootstrap-navbar">
        <div class="container">
            <div class="navbar-header">
                <div class="btn-more js-btn-more visible-xs">
                    <a href="#"><i class="icon-dots-three-vertical "></i></a>
                </div>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html" title="ProBootstrap:Enlight">Enlight</a>
            </div>

            <div id="navbar-collapse" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li <?php if ($page == 'index.php'){echo 'class="active"';} ?> ><a href="index.php">Home</a></li>
                    <li <?php if ($page == 'courses.php'){echo 'class="active"';} ?> ><a href="courses.php">Courses</a></li>
                    <li <?php if ($page == 'teachers.php'){echo 'class="active"';} ?> ><a href="teachers.php">Teachers</a></li>
                    <li <?php if ($page == 'events.php'){echo 'class="active"';} ?> ><a href="events.php">Events</a></li>
                    <li <?php if ($page == 'news.php'){echo 'class="active"';} ?> ><a href="news.php">News</a></li>
                    <li <?php if ($page == 'gallery.php'){echo 'class="active"';} ?>><a href="gallery.php">Gallery</a></li>
                    <li <?php if ($page == 'contact.php'){echo 'class="active"';} ?> ><a href="contact.php">Contact</a></li>
                    <li <?php if ($page == 'about.php'){echo 'class="active"';} ?> ><a href="about.php">About Us</a></li>
                </ul>
            </div>
        </div>
    </nav>
